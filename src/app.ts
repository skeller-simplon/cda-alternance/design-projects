// import { Character } from "./core/character";
// import { Potion } from "./core/potion";
// import { Berserk } from "./decorator/berserk";
// import { HpDrain } from "./decorator/hp-drain";
// import { Mighty } from "./decorator/mighty";
// import { SpellBook } from "./strategy/spellbook";
// import { Sword } from "./strategy/sword";


// const char1 = new Character(new HpDrain(new Mighty(new Sword())));

// const char2 = new Character(new Berserk(new SpellBook()));

// const popo = new Potion(char2);

// char2.addObserver(popo);


// char1.attack(char2);
// char1.attack(char2);
// char1.attack(char2);
// char1.attack(char2);
// char1.attack(char2);
// char1.hp = 100;
// char2.attack(char1);

// console.log(char1);



import 'reflect-metadata';

import { Controller } from "./di/controller";
import { inject } from "./di/injector";


const controller = inject(Controller);

console.log(controller);
