import { Character } from "../core/character";
import { IWeapon } from "./iweapon";



export class Sword implements IWeapon {
    private damage = 10;
    protected holder:Character;
    

    constructor() {}


    attack(target: Character): string {
        target.hp -= this.damage;
        this.holder.stamina -= 5;
        return "Attacked "+target.name+" with a sword";
    }

    setHolder(holder:Character) {
        this.holder = holder;
    }
}