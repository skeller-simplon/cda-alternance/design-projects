

import { Character } from "../core/character";
import { IWeapon } from "./iweapon";


export class SpellBook implements IWeapon {
    protected holder:Character;

    constructor() {}


    attack(target: Character): string {
        target.hp -= 7;
        this.holder.mana -= 5;
        return "Attacked "+target.name+" with a spell";
    }

    setHolder(holder:Character) {
        this.holder = holder;
    }
}