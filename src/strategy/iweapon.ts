import { Character } from "../core/character";

export interface IWeapon {
    attack(target:Character): string;
    setHolder(holder:Character): void;
}