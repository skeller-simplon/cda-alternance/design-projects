import { ICommand } from "./icommand";
import { Lamp } from "./lamp";


export class CommandLampToggle implements ICommand {

    constructor(private lamp: Lamp) { }

    execute(): void {
        if (this.lamp.isOn()) {
            this.lamp.turnOff();
        } else {
            this.lamp.turnOn();
        }
    }

}