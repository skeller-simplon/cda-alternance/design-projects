import { Character } from "../../core/character";
import { ICommandGame } from "./icommand-game";


export class CommandSelectCharacter implements ICommandGame {
    constructor(private toSelect:Character){}

    execute(): ICommandGame[] {
        return [
            // new CommandAttack(this.toSelect),
            // new CommandUnselect(),
            // new CommandMoveUp(this.toSelect)
        ];
    }
    getName(): string {
        return "Select "+this.toSelect.name;
    }

    

}