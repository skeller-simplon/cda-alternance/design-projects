import { ICommand } from "./icommand";
import { Vacum } from "./vacum";


export class CommandStartVacum implements ICommand {

    constructor(private vacum: Vacum) { }

    execute(): void {
        this.vacum.start();
    }

}