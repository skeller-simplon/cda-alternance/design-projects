
export class Lamp {
    private on = false;

    isOn() {
        return this.on;
    }

    turnOn() {
        this.on = true;
    }

    turnOff() {
        this.on = false;
    }

}