import { ICommand } from "./icommand";


export class Remote {
    commands: ICommand[] = [];


    pressButton(index:number) {
        this.commands[index].execute();
    }


}