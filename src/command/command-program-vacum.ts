import { ICommand } from "./icommand";
import { Vacum } from "./vacum";


export class CommandProgramVacum implements ICommand {
    private curHour = 0;
    constructor(private vacum: Vacum = new Vacum()) { }

    execute(): void {
        this.curHour++;
        if(this.curHour > 23) {
            this.curHour = 0
        }
        this.vacum.program(this.curHour);
    }

}