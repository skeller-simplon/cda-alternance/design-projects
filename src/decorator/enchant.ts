import { Character } from "../core/character";
import { IWeapon } from "../strategy/iweapon";


export abstract class Enchant implements IWeapon {
    protected holder: Character;
    private state: IState;
    constructor(protected enchanted: IWeapon) {
        this.state = new ActivatedState((target) => this.enchanted.attack(target));
    }

    attack(target: Character): string {
        return this.state.use(target);
    }

    setHolder(holder: Character): void {
        this.holder = holder;
        this.enchanted.setHolder(holder);
    }

    deactivate() {
        this.state = new DeactivatedState();
    }

    setStateAsMagic() {
        this.state = new MagicEnchantState((target: Character) => {
            this.holder.mana -= 5
            this.enchanted.attack(target)
        })
    }

}

interface IState {
    action: Function
    use(target: Character): string;
}

class ActivatedState implements IState {
    action: Function;

    constructor(action: Function) {
        this.action = action
    }

    use(target: Character) {
        return this.action(target)
    }
}

class DeactivatedState implements IState {
    action: Function;

    constructor(action: Function = () => console.log("L'objet est inactif")) {
        this.action = action;
    }

    use(target: Character) {
        return this.action(target)
    }
}

class MagicEnchantState implements IState {
    action: Function;
    constructor(action) { }

    use(target: Character) {
        return this.action()
    }
}