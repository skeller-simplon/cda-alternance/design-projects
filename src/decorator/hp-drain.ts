import { Character } from "../core/character";
import { Enchant } from "./enchant";


export class HpDrain extends Enchant {

    attack(target:Character):string {
        this.holder.hp += 5;
        return this.enchanted.attack(target);
    }
}