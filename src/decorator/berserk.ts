import { Character } from "../core/character";
import { IObserver } from "../observer/iobserver";
import { Enchant } from "./enchant";

export class Berserk extends Enchant implements IObserver{
    
    private berserkOn = false;

    attack(target:Character):string {  
        if(this.berserkOn)       {
            target.hp -= 10;
        }
        target.hp -= 5;
        return super.attack(target);
    }
    /**
     * Ici on surcharge le setHolder pour faire en sorte
     * d'inscrire l'enchantement en observateur du personnage
     * qui l'équipe
     */
    setHolder(holder:Character) {
        super.setHolder(holder);
        holder.addObserver(this);
    }
    /**
     * Dès que les hp du persos tombent en dessous d'une certaine
     * valeur, les bonus de l'enchantement s'activent et lui 
     * font faire des trucs en plus (ici, plus de dégât)
     */
    update(state: any): void {
        if(state.hp < 50) {
            this.berserkOn = true;
        } else {
            this.berserkOn = false;
        }
    }
}