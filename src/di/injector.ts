

//don't use in real life
export function inject(target) {

    const args = Reflect.getMetadata('design:paramtypes', target) || [];
    const instances = args.map(arg => inject(arg));

    return new target(...instances);
}