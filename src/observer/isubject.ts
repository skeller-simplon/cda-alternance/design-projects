import { IObserver } from "./iobserver";


export interface ISubject {
    addObserver(obs:IObserver):void;
    removeObserver(obs:IObserver): void;
    notifyObservers(): void;
}