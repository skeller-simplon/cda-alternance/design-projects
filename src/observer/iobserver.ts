

export interface IObserver {
    update(state):void;
}