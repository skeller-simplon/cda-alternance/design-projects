import { IObserver } from "./iobserver";
import { ISubject } from "./isubject";


export class Subject implements ISubject {
    private observers:IObserver[] = [];
    private name:string;

    public setName(name) {
        this.name = name;

        this.notifyObservers();
    }


    addObserver(obs: IObserver): void {
        if(!this.observers.includes(obs)) {
            this.observers.push(obs);
        }
    }
    removeObserver(obs: IObserver): void {
        this.observers = 
                this.observers.filter(item => obs === item);
    }
    notifyObservers(): void {
        for (const obs of this.observers) {
            obs.update(this.name);
        }
    }

}