// Un personnage peut attaquer

import { IObserver } from "../observer/iobserver";
import { ISubject } from "../observer/isubject";
import { IWeapon } from "../strategy/iweapon";

// Plusieurs manière d'attaque possibles : sort, une épée
// avec un arc. Mais on pourrait en avoir d'autres par la suite



// Créer une interface IWeapon qui aura une méthode attack(target:Character)

// Créer les différentes Weapons concrètes (Sword,Bow,Spellbook) qui auront chacune
// leur comportement d'attaque spécifique. (on peut dire par exemple que sword et bow 
// utilisent de la stamina, et spellbook utilise du mana du perso)

// Faire en sorte que quand l'perso attaque, il utilise l'attaque de son arme

// Pourquoi pas ensuite rajouter une méthode canAttack(target:Character) qui vérifie
// si la cible est dans le range de l'arme actuelle (1 case autour du perso pour la Sword,
// en forme de + pour l'arc, et n'importe où pour le sort)


export class Character implements ISubject {
    private _hp = 100;
    private _stamina = 100;
    private _mana = 100;
    private observers: IObserver[] = [];
    name: string;


    constructor(protected weapon: IWeapon) {
        weapon.setHolder(this);
    }


    get hp() {
        return this._hp;
    }
    get mana() {
        return this._mana;
    }
    get stamina() {
        return this._stamina;
    }
    set hp(hp: number) {
        this._hp = hp;
        this.notifyObservers();
    }
    set stamina(stamina: number) {
        this._stamina = stamina;
        this.notifyObservers();
    }
    set mana(mana: number) {
        this._mana = mana;
        this.notifyObservers();
    }

    attack(target: Character) {
        console.log(this.weapon.attack(target));
    }



    addObserver(obs: IObserver): void {
        if (!this.observers.includes(obs)) {
            this.observers.push(obs);
        }
    }
    removeObserver(obs: IObserver): void {
        this.observers =
            this.observers.filter(item => obs === item);
    }
    notifyObservers(): void {
        for (const obs of this.observers) {
            obs.update({
                hp: this.hp,
                stamina: this.stamina,
                mana: this.mana
            });
        }
    }
}
