import { IObserver } from "../observer/iobserver";
import { Character } from "./character";


export class Potion implements IObserver{
    private usage = 5;

    constructor(public receiver:Character){}

    update(state: any): void {
        if(state.hp < 25) {
            this.use();
        }
    }

    use() {
        if(this.usage > 0) {
            this.receiver.hp += 20;
            this.usage--;
        }
    }



}